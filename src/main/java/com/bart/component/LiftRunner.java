package com.bart.component;

import com.bart.ui.ButtonPane;


/**
 * Created by balabaev on 10.01.2018.
 */

/**
 * Управление лифтом
 */
public class LiftRunner implements Runnable{

    private Lift lift;
    private ButtonPane liftPane;
    private ButtonPane innerPane;
    private ButtonPane outterPane;

    private Integer maxFloors;
    private Integer floorHeight;
    private Integer liftSpeed;
    private Integer delayDoorTime;

    public LiftRunner(Lift lift,ButtonPane liftPane, ButtonPane innerPane, ButtonPane outterPane,
                      Integer maxFloors, Integer floorHeight, Integer liftSpeed, Integer delayDoorTime){
        this.lift = lift;
        this.liftPane = liftPane;
        this.innerPane = innerPane;
        this.outterPane = outterPane;

        this.maxFloors = maxFloors;
        this.floorHeight = floorHeight;
        this.liftSpeed = liftSpeed;
        this.delayDoorTime = delayDoorTime;
    }

    public void run(){
        int floor = lift.getCurrFloor();
        int prev = floor;
        //int delta = 1;
        lift.moveToFloor(floor);
        liftPane.on(floor);//стоим на начальном этаже

        Integer floorDelay = Math.round((float)floorHeight / (float)liftSpeed * 1000);
        while (!Thread.currentThread().isInterrupted()) try {

            floor = lift.getNextFloor();
            int i = prev;
            int delta = floor > prev ? 1 : floor < prev ? - 1 : 0;
            //поехали на следующий этаж в очереди
            boolean moveFlag = true;
            while (moveFlag) {

                if (delta != 0) {
                    Thread.sleep(floorDelay);
                    i += delta;
                }

                liftPane.setText(prev, "" + prev);
                liftPane.off(prev);
                liftPane.on(i);


                //если мы приехали на этаж и открыли двери
                if (lift.moveToFloor(i)){
                    lift.openDoor();
                    liftPane.openDoor(i);
                    liftPane.setText(i, i + ". Открылись двери");
                    Thread.sleep(delayDoorTime);
                    lift.closeDoor();
                    liftPane.setText(i, i + "");
                    liftPane.on(i);
                    innerPane.off(i);
                    outterPane.off(i);
                    prev = i;
                    break;
                } else
                    liftPane.setText(i, i + "");

                prev = i;
                moveFlag = (i != floor && i > 0 && i <= maxFloors);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
