package com.bart.component;

import lombok.Data;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by balabaev on 09.01.2018.
 */

/**
 * Класс лифта
 */
@Data
public class Lift {

    private ReentrantLock locker;
    private Condition condition;

    private Integer currFloor;
    private boolean currOpenDoor;

    Set<Integer> innerButtons = new LinkedHashSet<>();
    Set<Integer> outterButtons = new LinkedHashSet<>();


    public Lift(){
        locker = new ReentrantLock(true); // создаем блокировку
        condition = locker.newCondition(); // получаем условие, связанное с блокировкой
        currFloor = 1;// начальны этаж
    }

    /**
     * Найдем следующий этаж, куда можем поехать
     * сначала ищем в списке внутренниъ кнопок, потом смотрим внешние кнопки
     * @return
     */
    public Integer getNextFloor(){
        locker.lock();
        try {
            //ждем пока не будет вызван лифт
            while (innerButtons.size() == 0 && outterButtons.size() == 0)
                condition.await();

            //смотрим в спсике внутренних кнопок, они приоритетные
            if (innerButtons.size() > 0) {
                Iterator iter = innerButtons.iterator();
                return (Integer) iter.next();

            //потом смотрим список внешних вызовов
            } else if (outterButtons.size() > 0) {
                Iterator iter = outterButtons.iterator();
                return (Integer) iter.next();
            }
        }catch (InterruptedException e){
            System.out.println(e.getMessage());
        }finally{
            locker.unlock();
        }
        return null;
    }

    /**
     * Выделяем нужный этаж, тушим кнопки вызова
     * @param floor
     * @return true - если есть остановка
     */
    public boolean moveToFloor(Integer floor) {

        boolean result = false;

        //проверим есть ли остановка
        locker.lock();
        try{
            currFloor = floor;
            result = (innerButtons.remove(floor) | outterButtons.remove(floor));
            System.out.println("Лифт проехал этаж: " + floor);
            // сигнализируем, что надо ехать
            //condition.signalAll();
        }
        finally{
            locker.unlock();
        }
        return result;
    }


    /**
     * Попытка добавить вызов в Set кнопок
     * @param buttons
     * @param floor
     * @return
     */

    private boolean addButtons(Set<Integer> buttons, Integer floor){
        locker.lock();

        try {
            if (floor.equals(currFloor) && currOpenDoor)
                return false;

            buttons.add(floor);

            // сигнализируем, что надо ехать
            condition.signalAll();
        }
        finally{
            locker.unlock();
        }
        return true;
    }

    /**
     * добавление вызова для внутренних кнопок
     * @param floor
     */
    public boolean addFloorByInnerButtons(Integer floor) {
        return addButtons(innerButtons, floor);
    }

    /**
     * Добавление вызова для внешних кнопок
     * @param floor
     */
    public boolean addFloorByOutterButtons(Integer floor) {

        return addButtons(outterButtons, floor);
    }

    /**
     * Открываются двери
     */
    public void openDoor(){
        locker.lock();
        try {
            currOpenDoor = true;
        } finally{
            locker.unlock();
        }
    }

    /**
     * Закрылись двери
     */
    public void closeDoor(){
        locker.lock();
        try {
            currOpenDoor = false;
        } finally{
            locker.unlock();
        }

    }


}
