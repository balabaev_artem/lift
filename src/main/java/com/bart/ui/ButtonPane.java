package com.bart.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by balabaev on 10.01.2018.
 */

public class ButtonPane extends Container{

    List<JButton> btnList = new ArrayList<>();

    public List<JButton> getBtnList() {
        return btnList;
    }

    public ButtonPane(int nbtn, boolean enable, String title, ActionListener actionListener){
        createUI(nbtn, enable, title, actionListener);
    }

    public void createUI(int nbtn, boolean enable, String title, ActionListener actionListener){
        btnList = new ArrayList<>(nbtn);

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        JLabel label = new JLabel(title);
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(label);

        for (int i = 0; i < nbtn; i++){
            btnList.add(addAButton("" + (i + 1), enable, actionListener));
        }

        for (int i = btnList.size() - 1; i >= 0; i--){
            this.add(btnList.get(i));
        }
    }

    private JButton addAButton(String text, boolean enable, ActionListener actionListener) {
        JButton button = new JButton(text);
        button.setAlignmentX(Component.CENTER_ALIGNMENT);
        button.setMaximumSize(new Dimension(300/*Integer.MAX_VALUE*/, button.getMinimumSize().height));
        button.setEnabled(enable);
        button.setForeground(Color.BLACK);
        button.setActionCommand(text);
        if (actionListener != null)
            button.addActionListener(actionListener);
        return button;
    }

    /**
     * Поставить выделение
     * @param floor
     */
    public void on(Integer floor){
        try {
            JButton button = btnList.get(floor - 1);
            button.setBackground(Color.YELLOW);
        } catch (Exception e){
        }
    }

    /**
     * Выделение открытых дверей
     * @param floor
     */
    public void openDoor(Integer floor){
        try {
            JButton button = btnList.get(floor - 1);
            button.setBackground(Color.GREEN);
        } catch (Exception e){
        }
    }

    /**
     * снять выделение
     * @param floor
     */
    public void off(Integer floor){
        try {
            JButton button = btnList.get(floor - 1);
            button.setBackground(null);
        } catch (Exception e){
        }
    }

    public void setText(Integer floor, String text){
        try {
            JButton button = btnList.get(floor - 1);
            button.setText(text);
        } catch (Exception e){
        }

    }


}
