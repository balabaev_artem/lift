package com.bart;

import com.bart.component.Lift;
import com.bart.component.LiftRunner;
import com.bart.ui.ButtonPane;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


@SpringBootApplication
public class LiftApplication extends JFrame{

	private Lift lift;
	private Thread liftThread;
	private ButtonPane liftPanel;
	private ButtonPane innerButton;
	private ButtonPane outterButton;
	private Integer maxFloors;
	private Integer floorHeight;
	private Integer liftSpeed;
	private Integer delayDoorTime;

	public LiftApplication(@Value("${floors}") Integer maxFloors,
						   @Value("${floor.height}") Integer floorHeight,
						   @Value("${lift.speed}") Integer liftSpeed,
						   @Value("${delay.door}") Integer delayDoorTime
						   ) {
		this.maxFloors = maxFloors;
		this.floorHeight = floorHeight;
		this.liftSpeed = liftSpeed;
		this.delayDoorTime = delayDoorTime;

		lift = new Lift();

		initUI();
		startLiftRunner();

	}

	private void initUI() {

		liftPanel = new ButtonPane(maxFloors, false, "Лифт", null);
		innerButton = new ButtonPane(maxFloors, true, "Внутренние кнопки", new InnerActionListener()) ;
		outterButton = new ButtonPane(maxFloors, true, "Внешние кнопки", new OutterActionListener());

		Container pane = this.getContentPane();
		pane.setLayout(new BoxLayout(pane, BoxLayout.X_AXIS));
		pane.add(liftPanel);
		pane.add(innerButton);
		pane.add(outterButton);

		setTitle("Движение лифта");

		setSize(910, 600);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

	}

	/**
	 * Запуск потока лифта
	 */

	private void startLiftRunner(){
		LiftRunner liftRunner = new LiftRunner(lift, liftPanel, innerButton, outterButton,
				maxFloors, floorHeight, liftSpeed, delayDoorTime);
		liftThread = new Thread(liftRunner);
		liftThread.start();
	}

	/**
	 * Обработчик кнопок внутренних кнопок
	 */
	public class InnerActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JButton button = (JButton) e.getSource();
			if (lift.addFloorByInnerButtons(Integer.parseInt(button.getText())))
				button.setBackground(Color.YELLOW);
		}
	}

	/**
	 * Обработчик кнопок внутренних кнопок
	 */
	public class OutterActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JButton button = (JButton) e.getSource();
			if (lift.addFloorByOutterButtons(Integer.parseInt(button.getText())))
				button.setBackground(Color.YELLOW);
		}
	}

	public static void main(String[] args) {

		ConfigurableApplicationContext ctx = new SpringApplicationBuilder(LiftApplication.class)
				.headless(false).run(args);

		EventQueue.invokeLater(() -> {
			LiftApplication ex = ctx.getBean(LiftApplication.class);
			ex.setVisible(true);
		});
	}
}
